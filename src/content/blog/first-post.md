---
title: 'Game5 le commencement ...'
description: "Mais qu'est-ce donc ?"
pubDate: 'June 17 2024'
heroImage: '/Capture2.png'
---

Comme toutes les histoires, cette histoire commence avec un rêve, le mien : celui de produire un jeu vidéal
(Ah, j'ai un humour nul et j'écris mal. C'est juste pour la mise en contexte)

Comme le chiffre cinq le laisse à penser dans "Game5" cette histoire s'est déjà produite, mais nous ne nous y attarderons pas aujourd'hui.
À la base, cette fois-ci, mon rêve me réveille une fois de plus. Et cette fois-ci, mon cerveau me propose la chose suivante :
"Vu que tu n'es pas capable d'aller au bout de tes projets, pourquoi ne pas en commencer un simple avec la mode du moment ChatGPT". Avec un peu de chance, il va tout faire à ta place."

Dans mon lit à 23 heures, j'allume donc ChatGPT et je commence à écrire mon prompt. Au bout de quelques minutes, j'obtiens le prompt suivant :
```
Génère moi le code en unity c# d'un jeu 2d ou l'on controlle un nombre de vaisseau entre 1 et 10.

Le jeu commence par une page d'acceuille ou l'on choisis un pseudo et une couleur. La couleur permettra d'identifier les vaisseaux du joueur.

Chaque vaisseau appartenant au joueur est déplaçable. Le joueur peut selectionner un vaisseau avec un clic droit. Pour déplacer un vaisseau on utilise un clic gauche. Un joueur ne peut pas déplacer les vaisseaux d'un autre joueur.

Chaque vaisseau posède 10 points de vie. Lorsque deux vaisseau n'appartenant pas au même joueur sont proche, ils perdent chacun un point de vie.

 Le jeu est multijoueur.

La carte est dans des dimensions limités. 

```
Pour plus de réalisme, je vous laisse le prompt sans correction, mais je vous assure que ChatGPT m'a compris. Bon ceci dit, il ne m'a pas fait de miracle. Et donc dans toute la logique des choses, je me suis mis à lire la doc de Godot.
Ça ne fait pas forcément sens ? Ah, mais c'est parce que j'ai oublié de préciser qu'un de mes meilleurs amis me rabat les oreilles depuis des années la puissance de Godot. J'avais déjà auparavant fait des tentative avec Unity et Unreal Engine, mais sans grand succès.

Une fois n'est pas coutume, j'ai commencé par ouvrir le manuelle de Godot. Eh bah oui, à 23 heures, je n'avais pas spécialement envie de sortir de mon lit allumer le PC pour commencer le projet.

Quelques jours et heures de documentation lue plus tard, je commence ce fameux projet. Objectif : des triangles qui se déplacent avec un HUD rudimentaire. J'obtiens rapidement un HUD où je peux sélectionner ma couleur et mon pseudo puis changer de scène et lancer mon jeu.

Super ! Il est content Gaëtan. Bon maintenant la partie où je me casse toujours les dents jusqu'à présent (Ouais, je ne vais souvent pas beaucoup plus loin.) le networking ...

Bon la doc n'est pas si prolifique que ça sur cette partie surtout qu'histoire de me simplifier la vie, je pars sur une optique de serveur dédié. Les MMO, j'adore, je ne touche que rarement à un jeu solo depuis que j'ai quitté le monde de la PSII (Outch ... 2005). Mais mine de rien avec les articles de blog de Godot, la doc officielle et quelques questions bien senti sur le Discord de la communauté et avec beaucoup de bidouille ça se prends facilement en main, comparativement à Unity.

J'en suis arrivé à un premier point ou la connexion, reconnexion est fonctionnelle. Le déplacement, le tir et la destruction d'un autre vaisseau sont fonctionnels. Il manque cependant la partie de respawn et un aspect password qui arrivera pour lui bien plus tard.

C'est ainsi que se clôture le premier chapitre de cette aventure. Envie de télécharger la bête ? C'est par [ici](https://draftcorporation.fr/download/Game5_v0.1.zip), mais il vous faudra démarrer deux clients et un serveur pour faire joujou. Si vous êtes arrivé sur ce blog, c'est que vous pouvez normalement me contacter sur Discord. Autrement, vous pouvez toujours me contacter à l'ancienne à cette adresse : draftcorporationfr@gmail.com

N'hésitez pas à re-passer de temps à autre sur le blog. Je mettrai la suite de cette histoire !