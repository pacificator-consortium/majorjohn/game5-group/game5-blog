---
title: 'Game5, bizarrement la suite'
description: "Alors quoi de neuf ?"
pubDate: 'November 11 2024'
heroImage: '/Capture3.png'
---

Plusieurs mois se sont écoulés et j'ai un peu avancé sur le jeu, j'essaie toujours de m'appuyer sur ChatGPT afin de découvrir son utilisation. Je n'ai pour l'instant pas réussi à générer de code que j'ai conservé tel quel, mais c'est un très bon canard.

J'ai beaucoup réfléchi sûr où je veux amener le jeu en termes de but et de gameplay. Beaucoup de questions restent toujours en suspens, mais j'ai commencé à coucher sur papier plusieurs mécaniques de gameplay. Mais étant une vraie girouette, je ne suis sûr que de peu d'entre elles.


Le jeu s'articulera autour de 3 nations qui cherchent à exploiter la puissance du soleil pour le besoin de leur propre planète. Le joueur est l'un des commandants de ces nations.


Voici ce que j'ai ajouté :

- Côté connexion, j'ai ajouté un check de version afin de ne pas avoir de problème, étrange suite à l'utilisation d'une ancienne version. J'ai aussi repris aussi l'idée de quelqu'un sur le fait d'utiliser un token pour verrouiller le pseudo d'un joueur. J'ai utilisé un token JWT, je ne l'ai pour l'instant pas extrait dans un api tiers, c'est le serveur de jeu qui s'en occupe directement (C'est un des raccourcis que j'ai pris.).
Un tableau des scores
- La persistance de l'univers. Je persiste pour l'instant dans un fichier json et la sauvegarde est faite toutes les X seconde, c'est une solution rudimentaire qui conviendra dans un premier temps.
- Les couleurs sont associées à des factions. Ça me permet de faire respawn sur la planète respective de chaque couleur.


Pour la suite je suis actuellement en train de réfléchir pour m'attaquer à l'essence même des combats dans le jeu. J'ai encore beaucoup de questionnement, notamment sur la finesse du controle de tir (Questionnement induit par le fait de savoir si on contrôle un unique vaisseau ou non)

Ces réflexions m'amènent également à faire des croquis de situation, j'explore donc la possibilité de dessiner mes propres Sprite. Cela sonne bien, mais pour l'instant la réalité est plus qu'une aventure.
La version 0.2 :

Vous pouvez retrouver la release [ici](https://gitlab.com/pacificator-consortium/majorjohn/game5-group/game5/-/releases/v0.2). Si vous avez envie de jetter un oeil au code c'est [ici](https://gitlab.com/pacificator-consortium/majorjohn/game5-group/game5).
Il y a maintenant un serveur persistant vous pouvez directement vous connecter avec l'ip : 5.135.181.214